# Deploying consul on a local docker environment

## Selection of the running mode:
The Consul Server docker container can be run in two different modes: Single or Cluster
During development Single mode can be used.

### Runing on Single Mode

Configuration:
Under /main/resources/deployment/docker, the file .env will point to the right configuration files under /main/config
The MODE variable entry can take two forms:
MODE=single

Running the Compose:
Excecute the following command to start the Service Registry container on the path /main/resources/deployment/docker
cd {project_path}/main/resources/deployment/docker
docker-compose -f docker-compose-single.yaml up -d

### Running on Cluster Mode
Configuration:
Under /main/resources/deployment/docker, the file .env will point to the right configuration files under /main/config
The MODE variable entry can take two forms:
MODE=cluster


Excecute the following command to start the Service Registry container on the path /main/resources/deployment/docker
cd {project_path}/main/resources/deployment/docker
docker-compose up -d


### Accessing the service
The container will be attached to the 'services' network and will open the port 8500 for external connections from the host machine
Connect to the web application using http://localhos:8500

The folders under /main/[config|data] will be mounted on the container and the configuration files will be read from /main/config/[single|cluster] depending on the mode chosen




## Cleanup
```
docker-compose rm -f -s
# docker rm -v $(docker ps -a -f name=consul -q)
docker volume prune
```