# total-deployment

This deployment description deploys all necessary services of the e-commerce application

**Prerequsites**
crete services-network in the docker environment first

## Package index
This is a simple docker-compose description. It includes all the services and discovery&registry component and database

###ecom-consul :
  Consul is used for the discovery and registry component
###ecom-dbs :
Mongo is used for the database server for the microservices databases
###xvy-service :
Each service is registered using its name. The environments are set for the consul ip and database ip as per service

## deploy
```
docker-compose up -d
```
